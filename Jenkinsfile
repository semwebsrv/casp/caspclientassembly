#!groovy

// https://github.com/jenkinsci/kubernetes-plugin
// https://pushbuildtestdeploy.com/jenkins-on-kubernetes-building-docker-images/
//
// N.B. docker:latest (19.03.9 as at time of writing) has an API incompatibility. Pinning to 18 for now
podTemplate( 
  containers:[ containerTemplate(name: 'docker',               image:'docker:18',      ttyEnabled:true, command:'cat'),
               containerTemplate(name: 'node-build-container', image:'node:12-alpine', ttyEnabled:true, command:'cat') ],
  volumes: [
    hostPathVolume(hostPath: '/var/run/docker.sock', mountPath: '/var/run/docker.sock')
  ]) 
{
  node(POD_LABEL) {

    stage ('checkout') {
      checkout_details = checkout scm
      println("Checkout info: ${checkout_details}");
    }

    stage('Build') { 
      container('node-build-container') {
        // We should set a .npmrc file here to use the local package proxy cache
        sh 'echo "registry=https://nexus.semweb.co/repository/semweb-npm/" > ./.npmrc'
        sh 'echo "timeout=90000" >> ././nprc'
        sh 'cat ./.npmrc'
        sh 'echo build number: $BUILD_NUMBER'
        sh 'npm install'
        sh 'npm run-script build' 
      }
    }
  
    // https://www.jenkins.io/doc/book/pipeline/docker/
    stage('Docker Image') { 
      container('docker') {
        // N.B. using def docker_image here restricts the scope to just this stage, without gives it global within the script
        docker_image = docker.build("casp-client")
      }
    }

    stage("Push image") {
      container('docker') {
        package_props = readJSON file: './package.json'
        constructed_tag = "build-${package_props?.version}-${checkout_details?.GIT_COMMIT?.take(12)}"

        println("Considering build tag : ${constructed_tag}");
        // Some interesting stuff here https://github.com/jenkinsci/pipeline-examples/pull/83/files
        docker.withRegistry('http://docker.semweb.co', 'semweb-nexus') {
          docker_image.push("latest")
        }
      }
    }

    // For rolling update see https://kubernetes.io/blog/2018/04/30/zero-downtime-deployment-kubernetes-jenkins/
    // https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#updating-a-deployment
    // https://stackoverflow.com/questions/53443441/update-kubernetes-deployment-with-jenkins
    // https://github.com/jenkinsci/kubernetes-cd-plugin
    // https://cloudogu.com/en/blog/continuous_delivery_4_en
    // https://next.nutanix.com/blog-40/creating-a-ci-cd-pipeline-with-nutanix-karbon-and-jenkins-part-2-33595
    // Apparently we want to trigger 
    // kubectl rollout restart deployment myapp
    // kubeconfigId - a credentials block with ~/.kube.config yaml pasted in - not just the password
    stage('Rolling Update') {
        kubernetesDeploy(
          kubeconfigId: 'kubeconfig2',
          configs: 'deployment.yml',
          dockerCredentials: [
            [ url:'http://docker.semweb.co', credentialsId:'semweb-nexus' ]
          ]
        )
    }

    stage('Reporting') {
      sh 'ls'
    }

  }
}
