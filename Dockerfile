FROM nginx:1.18-alpine
COPY ./build /var/www
COPY ./nginx/holoreact /var/www/holoreact
COPY ./nginx/nginx.conf /etc/nginx/nginx.conf
EXPOSE 80
ENTRYPOINT ["nginx","-g","daemon off;"]
