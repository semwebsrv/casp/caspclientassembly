import React, { Component } from 'react'

import HoloAppContainer from '@semweb/holoreact'
import CaspModuleComponent from './casp/CaspModuleComponent'

export default class App extends Component {
  render () {
    
    HoloAppContainer.getConfig().appName='CASP';
    HoloAppContainer.getConfig().modules.push({path: '/casp', component: CaspModuleComponent});

    console.log("AppConfig: %o", HoloAppContainer.getConfig() );
 
    HoloAppContainer.getConfig().tenantResolverFunction = async function (axios) {
      var result = null;

      if ( axios != null ) {
        result = await axios
          .post('/caspcore/tenantResolver', {method:'host', content:window.location.hostname})
          .catch( function(error) {
                    console.log("Unable to resolve tenant %o",error);
                    return 'alerthub'
                  })
          .then(function(response) {
                  var t = null;
                  if ( ( response.data != null ) &&
                       ( response.data.tenant != null ) ) {
                    t = response.data.tenant;
                  }
                  else {
                    t='alerthub'
                  }
                  return t;
                })
      }
      else {
        console.log("no server api available to resolve hostname into tenant");
        result = 'alerthub'
      }

      console.log("tenantResolverFunction returns %s",result);
      return result
    }

    return (
      <HoloAppContainer />
    )
  }
}

