import React, { Component } from 'react';
import { FormGroup, Label, Input, FormText, Button} from 'reactstrap';
import PropTypes from "prop-types";
import AsyncSelect from 'react-select/async';
import {inject} from "mobx-react";
import { SimpleReferenceTypedown, PartyResourceInfo } from '@semweb/holoreact'

// see https://github.com/JedWatson/react-select
// and https://jedwatson.github.io/react-select/
// https://react-select.com/async

class AlertDetailRecord extends Component {

  constructor(props) {
    super(props);
    this.state ={
      selectedOption:null
    };
  }

  handleChange = event => {
    console.log("handleChange %o",event);
    if (event.target != null) {
      this.props.updater(event.target.name, event.target.value);
    }
  }

  render() {
    console.log("AlertDetailRecord::render props.ctx:%o",this.props.ctx);

    if ( this.props.ctx != null ) {

      let is_new_record = this.props.ctx.id == null;

      return (
        <div>

          <FormGroup>
            <Label for="owner">Owning Context</Label>
            <SimpleReferenceTypedown
              isDisabled={!is_new_record}
              ctx={this.props.ctx}
              name='party.owner'
              linkResourceType={PartyResourceInfo}
              defaultQueryTerm="%"
              labelKey="name"
              idKey="id"
              value={this.props.ctx['owner']}
              updater={this.props.updater}
            />

          </FormGroup>

          <FormGroup>
            <Label for="alert.title">Alert Title</Label>
            <Input name="alert.title" id="title" placeholder="Alert Title" value={this.props.ctx.title} onChange={this.handleChange} />
          </FormGroup>

        </div>
      );
    }
    else {
      return (<h1>No data</h1>);
    }
  }

  static propTypes = {
    ctx: PropTypes.object,
    updater: PropTypes.func.isRequired
  }
}

export default AlertDetailRecord

