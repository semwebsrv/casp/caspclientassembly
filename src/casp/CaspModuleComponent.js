import React, { Component } from 'react'
import { inject, Provider } from 'mobx-react'
import { iosPeople } from 'react-icons-kit/ionicons/iosPeople'
import {
  Container,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  NavbarBrand,
  Navbar,
  Collapse,
  UncontrolledDropdown,
  Nav,
  NavbarText
} from 'reactstrap'
import { Link, Redirect, Route } from 'react-router-dom'
import { FormattedMessage } from 'react-intl'
import Icon from 'react-icons-kit'

import { ModuleList, CrudStore, RecordViewer, Crud}  from '@semweb/holoreact'

import AlertResourceInfo from './AlertResourceInfo'


const caspCrudStore = new CrudStore()

class CaspModuleComponent extends Component {
  static getModuleInfo() {
    return {
      label: 'casp.modulename',
      icon: iosPeople,
      resources: {
      },
      i18n:{
        'casp.modulename':'CAP Alerting',
        'alert.title':'Alert Title',
        'casp.alerts.menu':'Alerts',
        'casp.alerts.search':'Search Alerts',
        'casp.alerts.create': 'Create Alert'
      }

    }
  }

  constructor(props) {
    super(props)
  }

  render() {
    const SystemSelectedCrudComponent =
      ModuleList.getAppConfig().systemDefaults.CRUDComponent

    const isOpen = true

    console.log('Render CASP component')

    return (
      <Provider crudStore={caspCrudStore}>
        <Container fluid className='p-0'>
          <Navbar className='navbar navbar-expand-lg navbar-light bg-light'>
            <NavbarBrand>
              <Icon className='mr-1' icon={iosPeople} />
              <FormattedMessage id='holo.system.modulename' />
            </NavbarBrand>
            <Collapse isOpen={isOpen} navbar>
              <Nav className="mr-auto" navbar>

                <UncontrolledDropdown nav inNavbar>
                  <DropdownToggle nav caret>
                    <FormattedMessage id='casp.alerts.menu' defaultMessage='Alerts' description='Alerts' />
                  </DropdownToggle>

                  <DropdownMenu right>
                    <DropdownItem tag={Link} to='/system/contexts'>
                      <FormattedMessage id='casp.alerts.search' defaultMessage='Search' description='Search Alerts' />
                    </DropdownItem>
                    <DropdownItem tag={Link} to='/system/contexts/NEW'>
                      <FormattedMessage id='casp.alerts.create' defaultMessage='Create' description='Create new Alert' />
                    </DropdownItem>
                  </DropdownMenu>
                </UncontrolledDropdown>

              </Nav>
            </Collapse>
          </Navbar>
          <Container fluid className='mt-2'>
            <Route exact path='/casp' render={() => <Redirect to='/casp/alerts' />} />
            <Route exact path='/casp/alerts' render={(props) => (
                <SystemSelectedCrudComponent {...props} resourceInfo={AlertResourceInfo} detailsPanel={RecordViewer} />
              )}
            />
            <Route
              path='/casp/alerts/:id'
              render={(props) => (
                <RecordViewer {...props} resourceInfo={AlertResourceInfo} id={props.match.params.id} />
              )}
            />

          </Container>
        </Container>
      </Provider>
    )
  }
}

export default inject('holoAppStore')(CaspModuleComponent)
