import AlertDetailRecord from './AlertDetailRecord'

import {fnGetResultsPage, fnGetResource} from '@semweb/holoreact'

class AlertResourceInfo {

  static resourcePath() {
    return '/casp/alerts'
  }

  // The graphql resource type we will use for this resource
  static resourceType() {
    return 'alert'
  }

  // Graphql record creation complains bitterly if we send it an ID in a create request, even if it is null,
  // so we don't put an "id" in this record.
  static blankRecord() {
    return {
      alert: {
        id: '',
        title: '',
        owner: null
      }
    }
  }

  // The default shape to ask for when requesting a party record
  static defaultResourceShape() {
    return '{ id, title, owner { id, name } }'
  }

  // The default graphql query we use when allowing users to search for these resources
  static defaultQuery() {
    return 'query($q: String, $max:Int, $offset:Int) { findAlertBaseByLuceneQuery(luceneQueryString: $q, max:$max, offset:$offset, sort:"title") { totalCount results { id title owner { name } } } }'
  }

  static queryResultsAccessPath() {
    return 'findAlertBaseByLuceneQuery'
  }

  // Usually create<DomainClass>
  static createResourceMutation() {
    return 'createAlertBase'
  }

  // Usually <DomainClass>InputType
  static createResourceType() {
    return 'AlertBaseInputType'
  }

  static defaultRowPresentation() {
    return [
      { Header: 'Alert Id', accessor: 'id' },
      { Header: 'Alert Title', accessor: 'title' },
      { Header: 'Context', accessor: 'owner.name' }
    ]
  }

  static resourceEditComponent() {
    return AlertDetailRecord
  }

  /**
   * getResultsPage is a function that accepts a serverApi object and a list of query properties
   * and returns a promise that resolves into a page of search results. We just leverage the default
   * implementation provided by holoHelpers
   *
   * @param holoServerApi
   * @param queryProps
   * @returns {*}
   */
  static getResultsPage(holoServerApi, queryProps) {
    // return fnGetResultsPage('/caspcore/graphql', holoServerApi, queryProps, this.defaultQuery(), 'findPartyByLuceneQuery')
    return fnGetResultsPage(holoServerApi.loadedConfig.coreModulePathPrefix+'/graphql', holoServerApi, queryProps, this.defaultQuery(), 'findAlertBaseByLuceneQuery')
  }

  static getResource(holoServerApi, id ) {
    // return  fnGetResource('/caspcore/graphql', holoServerApi, id, 'getParty', this.defaultResourceShape() )
    return  fnGetResource(holoServerApi.loadedConfig.coreModulePathPrefix+'/graphql', holoServerApi, id, 'getAlertBase', this.defaultResourceShape() )
  }
}


export default AlertResourceInfo
